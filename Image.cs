class Image : Componente
{
    public string Src { get; set; }

    public override string GetValue() {
        return Src;
    } 
}