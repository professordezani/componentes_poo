class Text : Componente
{
    public string Texto { get; set; }

    public Text(string texto) {
        Texto = texto;
    }

    public override string GetValue() {
        return Texto;
    }    
}