﻿Text txtSalvar = new Text("Salvar");

Button btnSalvar = new Button();
btnSalvar.Item = txtSalvar;
Console.WriteLine(btnSalvar.Item.GetValue());


Image imgDelete = new Image();
imgDelete.Src = "https://www.png";

Button btnApagar = new Button();
btnApagar.Item = imgDelete;
Console.WriteLine(btnApagar.Item.GetValue());

// Componente c1 = new Text("Salvar");
// Text t1 = c1 as Text;
// t1.Texto = "Hello, World";

// Componente c2 = new Image();
// Image i2 = (Image)c2;
// i2.Src = "image.gif";

// Text? t2 = c2 as Text;
