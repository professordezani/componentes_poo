class Button
{
    public Componente Item { get; set; }
    public Color Color { get; set; }
    public double Width { get; set; }
    public double Height { get; set; }
}

// class Button
// {
//     // Atributos:
//     private string text;
//     private Color color;
//     private double width;

//     // propriedade automática:
//     public double Height { get; }

//     // construtor:
//     public Button(string text) 
//     {
//         this.text = text;
//     }

//     public Button(string text, Color color)
//     {
//         this.text = text;
//         this.color = color;
//     }

//     // métodos:
//     public Color GetColor()
//     {
//         return this.color;
//     }

//     public void SetColor(Color color)
//     {
//         this.color = color;
//     }

//     public string GetText()
//     {
//         return this.text;
//     }

//     public void SetText(string text)
//     {
//         if(text.Length < 3) 
//         {
//             throw new Exception("Quantidade de caracteres < 3");
//         }

//         this.text = text;
//     }

//     // Propriedade:
//     public double Width
//     {
//         get { return width; }
//         set { width = value; }
//     }
// }